#-------------------------------------------------------------------------------
# DEFAULTS
#-------------------------------------------------------------------------------

TZ 		  := UTC
LC_ALL    := C.UTF-8
SHELL     := bash
MAKEFLAGS += --warn-undefined-variables
MAKEFLAGS += --no-builtin-rules

.ONESHELL:
.SHELLFLAGS := -eu -o pipefail -c
.DELETE_ON_ERROR:
.RECIPEPREFIX = >

#-------------------------------------------------------------------------------
# VARIABLES
#-------------------------------------------------------------------------------

DATE=$(shell date -u +%F)

PODMAN = podman
PODMANFLAGS = build --build-arg BUILDDATE=$(DATE) --no-cache --tag

DOCKER = docker
DOCKERFLAGS = build --build-arg BUILDDATE=$(DATE) --no-cache --tag

PREFIX = e2dk4r

#-------------------------------------------------------------------------------
# RULES
#-------------------------------------------------------------------------------

all: firefox torbrowser libreoffice gimp

firefox:
#> $(DOCKER) $(DOCKERFLAGS) $(PREFIX)/$@ $@
> $(PODMAN) $(PODMANFLAGS) $(PREFIX)/$@ $@

torbrowser:
#> $(DOCKER) $(DOCKERFLAGS) $(PREFIX)/$@ $@
> $(PODMAN) $(PODMANFLAGS) $(PREFIX)/$@ $@

libreoffice:
#> $(DOCKER) $(DOCKERFLAGS) $(PREFIX)/$@ $@
> $(PODMAN) $(PODMANFLAGS) $(PREFIX)/$@ $@

gimp:
#> $(DOCKER) $(DOCKERFLAGS) $(PREFIX)/$@ $@
> $(PODMAN) $(PODMANFLAGS) $(PREFIX)/$@ $@

inkscape:
#> $(DOCKER) $(DOCKERFLAGS) $(PREFIX)/$@ $@
> $(PODMAN) $(PODMANFLAGS) $(PREFIX)/$@ $@

keepassxc:
#> $(DOCKER) $(DOCKERFLAGS) $(PREFIX)/$@ $@
> $(PODMAN) $(PODMANFLAGS) $(PREFIX)/$@ $@

obs-studio:
#> $(DOCKER) $(DOCKERFLAGS) $(PREFIX)/$@ $@
> $(PODMAN) $(PODMANFLAGS) $(PREFIX)/$@ $@

blender:
#> $(DOCKER) $(DOCKERFLAGS) $(PREFIX)/$@ $@
> $(PODMAN) $(PODMANFLAGS) $(PREFIX)/$@ $@

deemix:
#> $(DOCKER) $(DOCKERFLAGS) $(PREFIX)/$@ $@
> $(PODMAN) $(PODMANFLAGS) $(PREFIX)/$@ $@

.PHONY: all firefox torbrowser libreoffice gimp inkscape blender keepassxc obs-studio deemix
